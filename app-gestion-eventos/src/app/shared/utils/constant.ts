export class Constant {
  static VERSION = 'v0.0.0';

  //  Regular expressions of the fields
  static PATTERN_LETRAS = /^[A-Za-zñÑÁÉÍÓÚÜáéíóúü\s]+$/;
  static PATTERN_NUMEROS = /^\d+$/;
  static PATTERN_LETRAS_NUMEROS = /^[A-Za-z0-9]+$/;
  static PATTERN_CORREO =
    /^[a-z0-9+_-]+(?:\.[a-z0-9+_-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
  static PATTERN_CONTRASENA =
    /^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/;

  //Correo Validators
  static CAMPO_MINIMO_CORREO = 4;
  static CAMPO_MAXIMO_CORREO = 30;

  //Contraseña Validators
  static CAMPO_MINIMO_CONTRASENA = 8;
  static CAMPO_MAXIMO_CONTRASENA = 30;

  //Nombre Validators
  static CAMPO_MINIMO_NOMBRE = 4;
  static CAMPO_MAXIMO_NOMBRE = 30;

  //Documento Validators
  static CAMPO_MINIMO_DOCUMENTO = 4;
  static CAMPO_MAXIMO_DOCUMENTO = 30;

  //Telefono Validators
  static CAMPO_MINIMO_TELEFONO = 6;
  static CAMPO_MAXIMO_TELEFONO = 15;

  //  General form errors
  static ERROR_OBTENIENDO_INFORMACION = 'Error obteniendo la información.';
  'Todos los campos deben estar correctamente diligenciados.';
  static ERROR_CAMPO_INCOMPLETO =
    'Todos los campos deben estar correctamente diligenciados.';

  //  Form field errors
  static ERROR_CAMPO_REQUERIDO = 'Campo requerido';
  static ERROR_CAMPO_SOLO_LETRAS = 'Solo se permiten letras';
  static ERROR_CAMPO_SOLO_NUMEROS = 'Solo se permiten números';
  static ERROR_CAMPO_SOLO_NUMEROS_LETRAS = 'Solo se permiten números y letras';

  //Nombre Error
  static ERROR_CAMPO_MINIMO_NOMBRE = 'Este campo es de mínimo 4';
  static ERROR_CAMPO_MAXIMO_NOMBRE = 'Este campo es de mínimo 30';

  //Documento Error
  static ERROR_CAMPO_MINIMO_DOCUMENTO = 'Este campo es de mínimo 4';
  static ERROR_CAMPO_MAXIMO_DOCUMENTO = 'Este campo es de mínimo 30';

  //Telefono Error
  static ERROR_CAMPO_MINIMO_TELEFONO = 'Este campo es de mínimo 4';
  static ERROR_CAMPO_MAXIMO_TELFONO = 'Este campo es de mínimo 30';

  //Correo Error
  static ERROR_CAMPO_MINIMO_CORREO = 'Este campo es de mínimo 4';
  static ERROR_CAMPO_MAXIMO_CORREO = 'Este campo es de mínimo 50';
  static ERROR_CAMPO_CORREO_INVALIDO = 'El correo es inválido';
  static ERROR_CAMPO_CORREO_EXISTENTE = 'Este correo ya está en uso';

  //Contraseña Error
  static ERROR_CAMPO_MINIMO_CONTRASENA = 'Este campo es de mínimo 8';
  static ERROR_CAMPO_MAXIMO_CONTRASENA = 'Este campo es de mínimo 30';
  static ERROR_CAMPO_CONTRASENA_NO_CONCIDEN = 'Las contraseñas no conciden.';
}
