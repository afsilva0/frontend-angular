import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-slidebar',
  templateUrl: './slidebar.component.html',
  styleUrls: ['./slidebar.component.css'],
})
export class SlidebarComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  dirigirPerfil() {
    this.router.navigate(['/gestionEventos/perfil']);
  }

  dirigirEventos() {
    this.router.navigate(['/gestionEventos/evento']);
  }

  dirigirConfiguracion() {
    this.router.navigate(['/gestionEventos/configuracion']);
  }
}
