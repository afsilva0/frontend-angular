import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

// Import Modules
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';

// Import Componentes
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { SlidebarComponent } from './components/slidebar/slidebar.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [NavbarComponent, FooterComponent, SlidebarComponent],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    HttpClientModule,
  ],
  exports: [
    ReactiveFormsModule,
    NavbarComponent,
    FooterComponent,
    SlidebarComponent,
    HttpClientModule,
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {}
