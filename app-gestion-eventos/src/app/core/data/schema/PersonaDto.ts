import { GenericDto } from './GenericDto';
import { UsuarioDto } from './UsuarioDto';

export class PersonaDto extends GenericDto {
  codigoPersona?: number;
  nombre?: string;
  tipoDocumento?: String;
  documento?: String;
  telefono?: String;
  genero?: String;
  usuario?: UsuarioDto;

  constructor(init?: Partial<PersonaDto>) {
    super();
    if (init) {
      Object.assign(this, init);
    }
  }
}
