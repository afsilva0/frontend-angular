import { GenericDto } from './GenericDto';

export class UsuarioDto extends GenericDto {
  codigoUsuario?: number;
  username?: string;
  contrasena?: String;

  constructor(init?: Partial<UsuarioDto>) {
    super();
    if (init) {
      Object.assign(this, init);
    }
  }
}
