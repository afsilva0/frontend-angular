import { GenericDto } from './GenericDto';

export class ErrorDto extends GenericDto {
  code?: string;
  message?: string;
  recomen?: string;

  constructor(init?: Partial<ErrorDto>) {
    super();
    if (init) {
      Object.assign(this, init);
    }
  }
}
