import { GenericDto } from './GenericDto';

export class SesionLoginDto extends GenericDto {
  token?: string;
  codigoUsuario?: number;
  tokenExpira?: Date;

  constructor(init?: Partial<SesionLoginDto>) {
    super();
    if (init) {
      Object.assign(this, init);
    }
  }
}
