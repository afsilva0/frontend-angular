import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

// import Dtos
import { GenericDto } from '../data/schema/GenericDto';

// import Services
import { HttpService } from './http.service';
import { LocalStorageService } from './localStorage.service';
import { AlertasService } from './alertar.service';
import { PersonaDto } from '../data/schema/PersonaDto';

@Injectable({
  providedIn: 'root',
})
export class PersonaService extends HttpService<GenericDto> {
  constructor(
    public httpClient: HttpClient,
    public router: Router,
    public alertasService: AlertasService,
    public localStorage: LocalStorageService
  ) {
    super(httpClient, environment.BaseUrl + 'persona', router, alertasService);
  }

  registrarse(personaDto: PersonaDto) {
    return super.post('registrarse', 'Error al registrse', personaDto);
  }
}
