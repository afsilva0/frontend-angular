import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

// import Dtos
import { GenericDto } from '../data/schema/GenericDto';
import { UsuarioDto } from '../data/schema/UsuarioDto';
import { SesionLoginDto } from '../data/schema/SesionLoginDto';

// import Services
import { HttpService } from './http.service';
import { LocalStorageService } from './localStorage.service';
import { AlertasService } from './alertar.service';

@Injectable({
  providedIn: 'root',
})
export class AutenticacionService extends HttpService<GenericDto> {
  sesionLoginDto: SesionLoginDto;

  constructor(
    public httpClient: HttpClient,
    public router: Router,
    public alertasService: AlertasService,
    public localStorage: LocalStorageService
  ) {
    super(
      httpClient,
      environment.BaseUrl + 'autenticacion',
      router,
      alertasService
    );
  }

  autenticacion(usuario: UsuarioDto) {
    return super.post('iniciar-sesion', 'Error al iniciar sesion', usuario);
  }

  postAutenticacion(sesionLoginDto: SesionLoginDto) {
    this.sesionLoginDto = {
      token: btoa(sesionLoginDto.token),
      codigoUsuario: sesionLoginDto.codigoUsuario,
      tokenExpira: sesionLoginDto.tokenExpira,
    };
    this.localStorage.removeItem('sesion');
    this.localStorage.setItem('sesion', this.sesionLoginDto);
  }

  cerrarSesion(usuario: UsuarioDto) {
    return super.post('cerrar-sesion', 'Error al cerrar sesion', usuario);
  }
}
