import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

// import Dtos
import { ErrorDto } from '../data/schema/ErrorDto';

@Injectable({
  providedIn: 'root',
})
export class AlertasService {
  constructor() {}

  toast(icon: SweetAlertIcon, title: string, timer: number = 3000) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: timer,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    Toast.fire({
      icon: icon,
      title: title,
    });
  }

  fireError(errorDto: ErrorDto) {
    Swal.fire({
      title: `Error [${errorDto.code}]`,
      html: `<b>Descripción:</b> ${errorDto.message}   </br>  <b>Recomendaciones:</b> ${errorDto.recomen} `,
      icon: 'error',
      showClass: {
        popup: 'animate__animated animate__fadeInDown',
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp',
      },
    });
  }
}
