import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

// import Dtos
import { GenericDto } from '../data/schema/GenericDto';

// import Services
import { AlertasService } from './alertar.service';

@Injectable({
  providedIn: 'root',
})
export class HttpService<T> {
  constructor(
    protected httpClient: HttpClient,
    protected url: string,
    protected router: Router,
    protected alertasService: AlertasService
  ) {}

  protected changeUrl(newUrl: string) {
    this.url = newUrl;
  }

  protected get(
    endpoint: string,
    errorMsg: string,
    config?: {}
  ): Observable<GenericDto> {
    return this.httpClient
      .get<GenericDto>(`${this.url}/${endpoint}`, config)
      .pipe(
        map((response: GenericDto) => {
          if (response.status == 500) {
            throw new Error(JSON.stringify(response.payload));
          }
          if (response.status == 200) {
            return response.payload;
          }
        }),
        catchError((error) => {
          if (error instanceof HttpErrorResponse) {
            this.alertasService.toast('error', errorMsg);
          } else {
            return throwError(error.message);
          }
        })
      );
  }

  protected post(
    endpoint: string,
    errorMsg: string,
    datos: T
  ): Observable<GenericDto> {
    return this.httpClient.post<T>(`${this.url}/${endpoint}`, datos).pipe(
      map((response: GenericDto) => {
        if (response.status == 500) {
          throw new Error(JSON.stringify(response.payload));
        }
        if (response.status == 200) {
          return response.payload;
        }
      }),
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          this.alertasService.toast('error', errorMsg);
        } else {
          return throwError(error.message);
        }
      })
    );
  }
}
