import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionNavbarComponent } from './gestion-navbar.component';

describe('GestionNavbarComponent', () => {
  let component: GestionNavbarComponent;
  let fixture: ComponentFixture<GestionNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionNavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
