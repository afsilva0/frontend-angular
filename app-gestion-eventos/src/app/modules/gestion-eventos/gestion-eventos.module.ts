import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { gestionEventosRoutingModule } from './gestion-eventos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GestionEventosComponent } from './gestion-eventos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { EventoComponent } from './evento/evento.component';
import { GestionNavbarComponent } from './gestion-navbar/gestion-navbar.component';

@NgModule({
  declarations: [
    GestionEventosComponent,
    PerfilComponent,
    ConfiguracionComponent,
    EventoComponent,
    GestionNavbarComponent,
    
  ],
  imports: [CommonModule, gestionEventosRoutingModule, SharedModule],
})
export class gestionEventosModule {}
