import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Import Dtos
import { PersonaDto } from '../../../core/data/schema/PersonaDto';

// Import Services
import { AlertasService } from 'src/app/core/services/alertar.service';
import { PersonaService } from 'src/app/core/services/persona.service';
import { NgxSpinnerService } from 'ngx-spinner';

// Import Constant
import { Constant } from 'src/app/shared/utils/constant';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  form: FormGroup;

  //cadenas para errores
  nombreError: string = '';
  tipoDocumentoError: string = '';
  documentoError: string = '';
  telefonoError: string = '';
  generoError: string = '';
  correoError: string = '';
  contrasenaError: string = '';
  confirmarContrasenaError: string = '';

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private alertasService: AlertasService,
    private personaService: PersonaService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      nombre: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_NOMBRE),
          Validators.maxLength(Constant.CAMPO_MAXIMO_NOMBRE),
          Validators.pattern(Constant.PATTERN_LETRAS),
        ],
      ],
      tipoDocumento: ['', [Validators.required]],
      documento: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_DOCUMENTO),
          Validators.maxLength(Constant.CAMPO_MAXIMO_DOCUMENTO),
          Validators.pattern(Constant.PATTERN_NUMEROS),
        ],
      ],

      telefono: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_TELEFONO),
          Validators.maxLength(Constant.CAMPO_MAXIMO_TELEFONO),
          Validators.pattern(Constant.PATTERN_NUMEROS),
        ],
      ],
      genero: ['', [Validators.required]],
      correo: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_CORREO),
          Validators.maxLength(Constant.CAMPO_MAXIMO_CORREO),
          Validators.pattern(Constant.PATTERN_CORREO),
        ],
      ],
      contrasena: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_CONTRASENA),
          Validators.maxLength(Constant.CAMPO_MAXIMO_CONTRASENA),
        ],
      ],
      confirmarContrasena: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_CONTRASENA),
          Validators.maxLength(Constant.CAMPO_MAXIMO_CONTRASENA),
        ],
      ],
    });
  }
  get nombre() {
    return this.form.get('nombre');
  }

  get tipoDocumento() {
    return this.form.get('tipoDocumento');
  }

  get documento() {
    return this.form.get('documento');
  }

  get telefono() {
    return this.form.get('telefono');
  }

  get genero() {
    return this.form.get('genero');
  }

  get correo() {
    return this.form.get('correo');
  }

  get contrasena() {
    return this.form.get('contrasena');
  }

  validarNombreCampo(): boolean {
    return this.nombre.errors && this.nombre.touched;
  }

  validarTipoDocumentoCampo(): boolean {
    return this.tipoDocumento.errors && this.tipoDocumento.touched;
  }

  validarDocumentoCampo(): boolean {
    return this.documento.errors && this.documento.touched;
  }

  validarTelefonoCampo(): boolean {
    return this.telefono.errors && this.telefono.touched;
  }

  validarGeneroCampo(): boolean {
    return this.genero.errors && this.genero.touched;
  }

  validarCorreoCampo(): boolean {
    return this.correo.errors && this.correo.touched;
  }

  validarContrasenaCampo(): boolean {
    return this.contrasena.errors && this.contrasena.touched;
  }

  validarNombreCampoValido(): boolean {
    return this.nombre.valid;
  }

  validarTipoDocumentoCampoValido(): boolean {
    return this.tipoDocumento.valid;
  }

  validarDocumentoCampoValido(): boolean {
    return this.documento.valid;
  }

  validarTelefonoCampoValido(): boolean {
    return this.telefono.valid;
  }

  validarGeneroCampoValido(): boolean {
    return this.genero.valid;
  }

  validarCorreoCampoValido(): boolean {
    return this.correo.valid;
  }

  validarContrasenaCampoValido(): boolean {
    return this.contrasena.valid;
  }

  validarNombre(): boolean {
    let status = false;
    if (this.nombre.dirty) {
      if (this.nombre.hasError('required')) {
        this.nombreError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.nombre.hasError('minlength')) {
        this.nombreError = Constant.ERROR_CAMPO_MINIMO_NOMBRE;
        status = true;
      } else if (this.nombre.hasError('maxlength')) {
        this.nombreError = Constant.ERROR_CAMPO_MAXIMO_NOMBRE;
        status = true;
      } else if (this.nombre.hasError('pattern')) {
        this.nombreError = Constant.ERROR_CAMPO_SOLO_LETRAS;
        status = true;
      }
    }

    return status;
  }

  validarTipoDocumento(): boolean {
    let status = false;
    if (this.tipoDocumento.dirty) {
      if (this.tipoDocumento.hasError('required')) {
        this.tipoDocumentoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      }
    }

    return status;
  }

  validarDocumento(): boolean {
    let status = false;
    if (this.documento.dirty) {
      if (this.documento.hasError('required')) {
        this.documentoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.documento.hasError('minlength')) {
        this.documentoError = Constant.ERROR_CAMPO_MINIMO_DOCUMENTO;
        status = true;
      } else if (this.documento.hasError('maxlength')) {
        this.documentoError = Constant.ERROR_CAMPO_MAXIMO_DOCUMENTO;
        status = true;
      } else if (this.documento.hasError('pattern')) {
        this.documentoError = Constant.ERROR_CAMPO_SOLO_NUMEROS;
        status = true;
      }
    }

    return status;
  }

  validarTelefono(): boolean {
    let status = false;
    if (this.telefono.dirty) {
      if (this.telefono.hasError('required')) {
        this.telefonoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.telefono.hasError('minlength')) {
        this.telefonoError = Constant.ERROR_CAMPO_MINIMO_TELEFONO;
        status = true;
      } else if (this.telefono.hasError('maxlength')) {
        this.telefonoError = Constant.ERROR_CAMPO_MAXIMO_TELFONO;
        status = true;
      } else if (this.telefono.hasError('pattern')) {
        this.telefonoError = Constant.ERROR_CAMPO_SOLO_NUMEROS;
        status = true;
      }
    }
    return status;
  }

  validarGenero(): boolean {
    let status = false;
    if (this.genero.dirty) {
      if (this.genero.hasError('required')) {
        this.generoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      }
    }
    return status;
  }

  validarCorreo(): boolean {
    let status = false;
    if (this.correo.dirty) {
      if (this.correo.hasError('required')) {
        this.correoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.correo.hasError('minlength')) {
        this.correoError = Constant.ERROR_CAMPO_MINIMO_CORREO;
        status = true;
      } else if (this.correo.hasError('maxlength')) {
        this.correoError = Constant.ERROR_CAMPO_MAXIMO_CORREO;
        status = true;
      } else if (this.correo.hasError('pattern')) {
        this.correoError = Constant.ERROR_CAMPO_CORREO_INVALIDO;
        status = true;
      }
    }

    return status;
  }

  validarContrasena(): boolean {
    let status = false;
    if (this.contrasena.dirty) {
      if (this.contrasena.hasError('required')) {
        this.contrasenaError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.contrasena.hasError('minlength')) {
        this.contrasenaError = Constant.ERROR_CAMPO_MINIMO_CONTRASENA;
        status = true;
      } else if (this.contrasena.hasError('maxlength')) {
        this.contrasenaError = Constant.ERROR_CAMPO_MAXIMO_CONTRASENA;
        status = true;
      }
    }

    return status;
  }

  limpiarFormulario(): void {
    this.form.reset();
  }

  dirigirHome(): void {
    this.router.navigate(['/gestionEventos']);
  }

  registrarse(): void {
    if (this.form.valid) {
      let persona = new PersonaDto();

      persona = {
        nombre: this.nombre.value,
        tipoDocumento: this.tipoDocumento.value,
        documento: this.documento.value,
        telefono: this.telefono.value,
        genero: this.genero.value,
        usuario: {
          username: this.correo.value,
          contrasena: this.contrasena.value,
        },
      };

      console.log(persona);

      /*    this.spinner.show();
      this.personaService.registrarse(persona).subscribe(
        (data) => {
          this.spinner.hide();
          this.limpiarFormulario();
          this.alertasService.toast('error', 'Registrado Exitosamente');
        },
        (err) => {
          this.spinner.hide();
          this.alertasService.toast('error', 'Error al Registrarse');
        }
      ); */
    } else {
      this.form.markAllAsTouched();
      this.alertasService.toast('error', Constant.ERROR_CAMPO_INCOMPLETO);
    }
  }
}
