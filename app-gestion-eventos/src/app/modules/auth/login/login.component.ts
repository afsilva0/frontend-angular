import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Import Dtos
import { UsuarioDto } from '../../../core/data/schema/UsuarioDto';

// Import Services
import { AlertasService } from 'src/app/core/services/alertar.service';
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { NgxSpinnerService } from 'ngx-spinner';

// Import Constant
import { Constant } from 'src/app/shared/utils/constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  //cadenas para errores
  correoError: string = '';
  contrasenaError: string = '';

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private alertasService: AlertasService,
    private spinner: NgxSpinnerService,
    private autenticacionService: AutenticacionService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      correo: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_CORREO),
          Validators.maxLength(Constant.CAMPO_MAXIMO_CORREO),
          Validators.pattern(Constant.PATTERN_CORREO),
        ],
      ],
      contrasena: [
        '',
        [
          Validators.required,
          Validators.minLength(Constant.CAMPO_MINIMO_CONTRASENA),
          Validators.maxLength(Constant.CAMPO_MAXIMO_CORREO),
        ],
      ],
    });
  }

  get correo() {
    return this.form.get('correo');
  }

  get contrasena() {
    return this.form.get('contrasena');
  }

  validarCorreoCampo(): boolean {
    return this.correo.errors && this.correo.touched;
  }

  validarContrasenaCampo(): boolean {
    return this.contrasena.errors && this.contrasena.touched;
  }

  validarCorreoCampoValido(): boolean {
    return this.correo.valid;
  }

  validarContrasenaCampoValido(): boolean {
    return this.contrasena.valid;
  }

  validarCorreo(): boolean {
    let status = false;
    if (this.correo.dirty) {
      if (this.correo.hasError('required')) {
        this.correoError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.correo.hasError('minlength')) {
        this.correoError = Constant.ERROR_CAMPO_MINIMO_CORREO;
        status = true;
      } else if (this.correo.hasError('maxlength')) {
        this.correoError = Constant.ERROR_CAMPO_MAXIMO_CORREO;
        status = true;
      } else if (this.correo.hasError('pattern')) {
        this.correoError = Constant.ERROR_CAMPO_CORREO_INVALIDO;
        status = true;
      }
    }

    return status;
  }

  validarContrasena(): boolean {
    let status = false;
    if (this.contrasena.dirty) {
      if (this.contrasena.hasError('required')) {
        this.contrasenaError = Constant.ERROR_CAMPO_REQUERIDO;
        status = true;
      } else if (this.contrasena.hasError('minlength')) {
        this.contrasenaError = Constant.ERROR_CAMPO_MINIMO_CONTRASENA;
        status = true;
      } else if (this.contrasena.hasError('maxlength')) {
        this.contrasenaError = Constant.ERROR_CAMPO_MAXIMO_CONTRASENA;
        status = true;
      }
    }

    return status;
  }

  limpiarFormulario(): void {
    this.form.reset();
  }

  dirigirHome() {
    this.router.navigate(['/gestionEventos']);
  }

  iniciarSesion() {
    if (this.form.valid) {
      let usuario = new UsuarioDto();

      usuario = {
        username: this.correo.value,
        contrasena: this.contrasena.value,
      };

      /*   this.spinner.show();
      this.autenticacionService.autenticacion(usuario).subscribe(
        (data) => {
          this.spinner.hide();
          this.autenticacionService.postAutenticacion(data);
          this.limpiarFormulario();
          this.router.navigate(['/gestionEventos']);
        },
        (err) => {
          this.spinner.hide();
          this.alertasService.toast('error', 'Inicio de sesión no válido');
        }
      ); */

      console.log(usuario);
    } else {
      this.form.markAllAsTouched();
      this.alertasService.toast('error', Constant.ERROR_CAMPO_INCOMPLETO);
    }
  }
}
