// Import Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

// Import Components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';

// Import Services
import { AutenticacionService } from 'src/app/core/services/autenticacion.service';
import { PersonaService } from 'src/app/core/services/persona.service';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, HomeComponent],
  imports: [CommonModule, AuthRoutingModule, SharedModule],
  providers: [AutenticacionService, PersonaService],
})
export class AuthModule {}
